<?php

use App\Http\Controllers\Api\Event\EventController;
use App\Http\Controllers\Api\Status\StatusController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resources([
    'events' => EventController::class
]);

Route::get('{event}/confirm',   [StatusController::class, 'approved']);
Route::get('{event}/unconfirm', [StatusController::class, 'disapproved']);
Route::get('{user_id}/events',  [StatusController::class, 'getThem']);
