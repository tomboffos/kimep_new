<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EventTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->post('/api/events',[
            'name' => '123TEST123',
            'time' => '22:30',
            'date' => Carbon::now(),
            'hall' => '313',
            'status_id' => 1
        ]);

        $response->assertStatus(201);
    }
}
