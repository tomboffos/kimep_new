<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $names = [
            'Under consideration',
            'Approved',
            'Disapproved'
        ];

        foreach ($names as $name)
        Status::insert([
                'name' => $name
            ]);

        // \App\Models\User::factory(10)->create();
    }
}
