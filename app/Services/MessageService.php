<?php


namespace App\Services;


use App\Models\Event;

class MessageService
{
    private $pushService;

    public function __construct()
    {
        $this->pushService = new PushService();
    }

    public function message(Event $event)
    {
        $this->pushService->send($event->device_token, 'Hall Reservation',
            'Your hall reservation request has been considered');
    }
}
