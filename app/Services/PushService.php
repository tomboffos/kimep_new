<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;

class PushService
{
    private $serverKey = '';

    public function send($device_tokens,string $title, string $text)
    {
        $response = Http::withHeaders([
            'Authorization' => "key=$this->serverKey",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_tokens,
            'notification' => [
                'title' => $title,
                'body' => $text,
                "sound" => 'notification.wav',
            ],
        ]);
//        Log::info('asd  '.$response->body());
        return $response->successful();
    }
}
