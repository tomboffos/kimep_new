<?php

namespace App\Http\Requests\General;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required|email',
            'time'         => 'required',
            'date'         => 'required',
            'hall'         => 'required',
            'status_id'    => 'required',
            'title'        => 'required',
            'device_token' => 'required',
            'user_id'      => 'required'
        ];
    }
}
