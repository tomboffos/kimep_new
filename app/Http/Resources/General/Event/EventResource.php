<?php

namespace App\Http\Resources\General\Event;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'title' => $this->title,
            'time' => $this->time,
            'date' => $this->date->format('d.m.Y'),
            'hall' => $this->hall,
            'status' => $this->status->name,
            'created_at' => $this->created_at->format('d:m:Y'),
            'user_id' => $this->user_id,
            'device_token' => $this->device_token



        ];
    }
}
