<?php

namespace App\Http\Controllers\Api\Status;

use App\Domains\Events\EventDomain;
use App\Http\Controllers\Controller;
use App\Http\Resources\General\Event\EventResource;
use App\Models\Event;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    private $eventDomain;

    public function __construct()
    {
        $this->eventDomain = new EventDomain();
    }

    public function approved(Event $event)
    {
        return $this->eventDomain->confirmEvent($event);
    }

    public function disapproved(Event $event)
    {
        return $this->eventDomain->unconfirmEvent($event);
    }

    public function getThem(Request $request)
    {
        return EventResource::collection(Event::with('status')->where('user_id', $request->user_id)->orderBy('created_at', 'desc')->get());
    }

}
