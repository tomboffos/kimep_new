<?php

namespace App\Http\Controllers\Api\Event;

use App\Domains\Events\EventDomain;
use App\Http\Controllers\Controller;
use App\Http\Requests\General\EventRequest;
use App\Http\Resources\General\Event\EventResource;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    private $eventDomain;

    public function __construct()
    {
        $this->eventDomain = new EventDomain();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        return $this->eventDomain->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
//     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Event $event)
    {
        $date = $event->date->format('d-m-Y');
        $url = $request->url();
        $jopa ='Event - '.$event->title.'; '.  'Hall - '.$event->hall.';  Date - '. $date .';  Time - '. $event->time.';';
        return view('welcome', compact('jopa', 'event', 'date', 'url'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
