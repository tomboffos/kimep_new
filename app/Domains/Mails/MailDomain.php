<?php


namespace App\Domains\Mails;


use App\Domains\Contracts\Mails\MailContract;
use App\Http\Requests\General\EventRequest;
use Illuminate\Support\Facades\Mail;

class MailDomain implements MailContract
{
    public function send(EventRequest $request, $event)
    {
        Mail::send('mail.mail', ['event' => $event] ,function ($message) {
            $message->to('mobapprove@kimep.kz', 'kimep.kz')->subject('Заявка');
            $message->from('mobile@kimep.kz','kimep.kz');
        });
        return response([
            'data' => $event
        ], 201);
    }
}
