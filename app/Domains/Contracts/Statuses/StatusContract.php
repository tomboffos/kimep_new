<?php


namespace App\Domains\Contracts\Statuses;


interface StatusContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME
    ];
}
