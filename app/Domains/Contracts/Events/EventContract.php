<?php


namespace App\Domains\Contracts\Events;


use App\Http\Requests\General\EventRequest;
use App\Models\Event;

interface EventContract
{
    const NAME         =   'name';
    const TIME         =   'time';
    const DATE         =   'date';
    const HALL         =   'hall';
    const STATUS_ID    =   'status_id';
    const TITLE        =   'title';
    const DEVICE_TOKEN =   'device_token';
    const USER_ID      =   'user_id';

    const FILLABLE = [
        self::NAME,
        self::TIME,
        self::DATE,
        self::HALL,
        self::STATUS_ID,
        self::TITLE,
        self::DEVICE_TOKEN,
        self::USER_ID
    ];

    public function store(EventRequest $request);

    public function confirmEvent($event);
    public function unconfirmEvent($event);

}
