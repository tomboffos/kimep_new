<?php


namespace App\Domains\Contracts\Mails;


use App\Http\Requests\General\EventRequest;

interface MailContract
{
    public function send(EventRequest $request, $event);
}
