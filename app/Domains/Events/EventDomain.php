<?php


namespace App\Domains\Events;


use App\Domains\Contracts\Events\EventContract;
use App\Domains\Mails\MailDomain;
use App\Http\Requests\General\EventRequest;
use App\Http\Resources\General\Event\EventResource;
use App\Models\Event;

class EventDomain implements EventContract
{
    private $mail;

    public function __construct()
    {
        $this->mail = new MailDomain();
    }

    public function store(EventRequest $request)
    {
        $event = Event::create($request->validated());

        $this->mail->send($request, $event);
        return response([
            'data' => new EventResource($event),
            'message' => 'SENT'
        ], 201);
    }

    public function confirmEvent($event)
    {
        $event->update([
            'status_id' => 2
        ]);
        return redirect()->back();
    }

    public function unconfirmEvent($event)
    {
        $event->update([
            'status_id' => 3
        ]);
        return redirect()->back();
    }
}
