<?php

namespace App\Models;

use App\Domains\Contracts\Events\EventContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = EventContract::FILLABLE;

    protected $dates = ['date'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function confirm($crud = false)
    {
        if ($this->attributes['status_id'] == 2)
            return "<a class='btn btn-sm btn-link' href='/api/events/" . $this->attributes['id'] . "' ><i class='la la-area-chart'></i>Generate QR</a>";
    }

    public function unconfirm($crud = false)
    {
        if ($this->attributes['status_id'] == 1)
            return "<a class='btn btn-sm btn-link' href='/api/" . $this->attributes['id'] . "/unconfirm' ><i class='la la-area-chart'></i>Disapprove</a>";
    }


}
